data "docker_registry_image" "Consul" {
	name = "consul:latest"
}

resource "docker_image" "Consul" {
	name = data.docker_registry_image.Consul.name
	pull_triggers = [data.docker_registry_image.Consul.sha256_digest]
	force_remove = false
	keep_locally = true
}

module "Consul" {
	source = "gitlab.com/RedSerenity/docker/local"

  name = "${var.node_prefix}.Consul"
  image = docker_image.Consul.latest

	networks = [{ name: var.docker_network, aliases: ["consul.${var.internal_domain_base}"] }]
  ports = [
		{ internal: 53, external: 53, protocol: "udp" },
		{ internal: 8500, external: 8500, protocol: "tcp" }
	]

	volumes = [
    {
      host_path = "${var.docker_data}/Consul"
      container_path = "/consul/data",
      read_only = false
    }
  ]

  command = ["agent", "-server", "-datacenter=AnderDC", "-dns-port=53", "-recursor=8.8.8.8", "-ui"]

	environment = {
		"CONSUL_CLIENT_INTERFACE": "eth0",
    "CONSUL_ALLOW_PRIVILEGED_PORTS": "true"
	}
}